# API Filtering

This project is a mockup that had allowed to get used with Typescript Development on the server side
In the development proccess I could get used with it's syntax and with some pattern concepts like Abstract Factory
and I've make use of the dependency injection technique with 2 levels of abstaction.

I've conducted some repository related unit testing and e2e tests with the supertest module, which
could be awesome when developing APIs.

Essentialy I've created two endponts:

- One of them allows us to upload and update repository data from a .csv file with product information

- The other is concerned by applying filtering according to the query params in order to
filter specific products

This architecture make use of an repository interface abstraction that is database agnostic.


# References:

[Extracting query params from express](https://stackabuse.com/get-query-strings-and-parameters-in-express-js/)
[Custom check](https://stackoverflow.com/questions/68880132/how-can-i-check-if-object-is-of-custom-type-in-typescript)
