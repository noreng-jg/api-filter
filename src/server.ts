import App from './router';
import Controllers from './controllers';
import Repository from './repositories';

const repository = new Repository('fake')
const controllers = new Controllers(repository);
const app = new App(controllers, 7070)

app.init();