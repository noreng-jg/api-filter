import crypto from 'crypto';

export default class Generator {
  public static async generateNumber() {
    return Math.floor(Math.random() * 1000000);
  }

  public static encryptPassword(password: string) {
    const hash = crypto.createHash('sha256');
    hash.update(password);
    return hash.digest('hex');
  }
}