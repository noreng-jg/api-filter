import { Request, Response } from 'express';
import { Product } from '../models/Product';

export default class FileController {
    private repo: any;

    constructor(repo: any) {
        this.repo = repo;
        this.readFileUploader = this.readFileUploader.bind(this)
    }

    public readFileUploader(req: Request, res: Response) {
        const file = req.file;

        if (!file) {
            throw new Error('Couldn\'t find the file in the request')
        }

        const fileContent = file.buffer.toString();

        const results = fileContent.split('\n').map((m)=>m.split(','));

        const products = results.map((p) => <Product>{
            id: parseInt(p[0]),
            name: p[1],
            price: parseInt(p[2]),
        });

        this.repo.products.createMany(products)

        res.send({
            'Message': "The products were added"
        })
    }
}