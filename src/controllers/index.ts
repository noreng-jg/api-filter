
import FileController from './file'
import ProductController from './product'

export default class Controllers {
    private file: any;
    private products: any;
    
    constructor(repository: any) {
        this.file = new FileController(repository); 
        this.products = new ProductController(repository)
    }
}