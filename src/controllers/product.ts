import { Response, Request } from 'express';
import { ProductParams, Product, getProductParamsFromQueryString } from '../models/Product';

export default class ProductController {
    private repo: any;

    constructor(repo: any) {
        this.repo = repo;
        this.getProducts = this.getProducts.bind(this)
    }

    public async getProducts(req: Request, res: Response) {
       let products: Array<Product> = [];

       const [ hasQuery, params ] = getProductParamsFromQueryString(req.query)

       if (!hasQuery) {
            // default pagination

            products = await this.repo.products.getProducts(<ProductParams>{
                    page: 1,
                    perPage: 10,
            });
       } else {
            products = await this.repo.products.getProducts(params)
       }

       return res.send({
            products,
       });
    }
}