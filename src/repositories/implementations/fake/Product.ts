import IProduct from '../../IProduct';
import { Product, ProductDTO, ProductParams } from '../../../models/Product';
import { Tag } from '../../../models/Tags';
import Generator from '../../../utils/generators'

export default class FakeProductRepository implements IProduct {
  private fakedb: Product[] = [];
  private logger: any;

  constructor(logger: any) {
    this.logger = logger;
  }

  public get data(): Product[] {
    return this.fakedb;
  }

  public setData(products: Product[]) {
    this.fakedb = [...this.data, ...products];
  }

  public async createProduct(product: ProductDTO): Promise<Product> {
    this.logger.debug('Creating product');

    let randomId: number;

    randomId = await Generator.generateNumber();

    let newProduct: Product = {
      id: randomId,
      name: product.name,
      price: product.price,
    };

    this.data.push(newProduct);

    return newProduct;
  }

  public async updateProduct(id: number, product: ProductDTO): Promise<Product> {
    this.logger.debug('Updating product');

    let index = this.data.findIndex((p: Product) => p.id == id);

    const newProduct = <Product>{
      id: this.data[index].id,
      name: product.name,
      price: product.price
    }

    this.data.splice(index, 1, newProduct);

    return newProduct;
  }

  public async getProductById(id: Number): Promise<Product> {
    this.logger.debug('Getting product by id');
    return this.data.filter((product: Product) => product.id === id)[0];
  }

  public async getProducts(params?: ProductParams): Promise<Product[]> {
    this.logger.debug('Getting products...')

    let result: Array<Product> = this.data;

    if (params) {
      this.logger.debug('Applying params filtering')

      result = this.data.slice(params.perPage*((params.page+1) -2), params.perPage*params.page)

      if (params.tags) {
        this.logger.debug('Tags filtering')

        const { tags } = params;

        const reduced = result.map((p) => p.tags?.reduce((ac: String[], t: Tag) => [t.name, ...ac], []))
        .filter(v => v !== undefined)

        let indexes: Array<Number> = [];

        let filteredProducts : Array<Product> = [];

        for (let i = 0; i < reduced.length; i++) {
            let r = reduced[i]
            r?.sort()
            tags.sort()

            const reducedString = JSON.stringify(r).replace('[', '').replace(']', '')
            const tagsString = JSON.stringify(tags).replace('[', '').replace(']', '')

            if (reducedString.includes(tagsString)) {
              indexes.push(i)
            }
        }

        for (let i = 0; i < indexes.length; i++) {
          filteredProducts.push(result[i])
        }

        result = filteredProducts;
      }

      if (params.search) {
        this.logger.debug('Search string filtering')

        let searchString = '';

        searchString = params.search;

        result = result.filter(p => p.name.includes(searchString))
      }
    }

    return result;
  }

  public async deleteProduct(id: number): Promise<void> {
    this.logger.debug('Deleting product...')
    let index = this.data.findIndex((p: Product) => p.id == id);
    this.data.splice(index, 1);
  }

  public async createMany(products: Product[]): Promise<Product[]> {
    this.logger.debug('Creating multiple products...');

    this.setData(products);

    return products;
  }
};
