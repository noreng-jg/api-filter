import { Product, ProductDTO, ProductParams } from '../models/Product';

export default interface IProduct {
  getProductById(id: number): Promise<Product>;
  getProducts(params?: ProductParams): Promise<Product[]>;
  createProduct(product: ProductDTO): Promise<Product>;
  updateProduct(id: number, product: ProductDTO): Promise<Product>;
  deleteProduct(id: number): Promise<void>;
  createMany(products: Product[]): Promise<Product[]>;
}