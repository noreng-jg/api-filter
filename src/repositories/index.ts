import FakeProductRepository from "./implementations/fake/Product";
import Logger from '../config/logger';

class Repository {
    public products: any;

    constructor(type: string) {

        switch (type) {
            case 'sql':
                
                break;
        
            default: // fake case
                this.products = new FakeProductRepository(Logger);

                break;
        }
    }
}

export default Repository;