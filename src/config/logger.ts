import pino from 'pino';

// use trace level config
const logger = pino({
  level: 'trace',
});

export default logger;