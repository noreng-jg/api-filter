import Repository from '../repositories'
import { Product, ProductDTO } from '../models/Product';
import Generator from '../utils/generators'

describe('Repository', () => {
    let repository: any;

    beforeAll(() => {
        repository = new Repository('fake');

        // mock Generator generateNumber function to return 1
        jest.spyOn(Generator, 'generateNumber').mockImplementation((): Promise<number> => {
            return Promise.resolve(1);
        });
    })

    it('Repository should be defined', () => {
        expect(repository).toBeDefined();
    })

    describe('Product repository', () => {
        it('Should be able to create new product', async () => {
            const product = await repository.products.createProduct(<ProductDTO>{
                name: 'product1',
                price: 123,
            })

            const results = await repository.products.getProducts();

            expect(results).toHaveLength(1);
        })

        it('Should be able to create another product', async () => {
            jest.spyOn(Generator, 'generateNumber').mockImplementation((): Promise<number> => {
                return Promise.resolve(2);
            });

            const product = await repository.products.createProduct(<ProductDTO>{
                name: 'product1',
                price: 123,
            })

            const results = await repository.products.getProducts();

            expect(results).toHaveLength(2);
        })

        it('Should be ablt to update product', async () => {
            let productDto = <ProductDTO>{
                price: 200,
                name: 'product2'
            }

            const product = await repository.products.updateProduct(1, productDto)

            expect(product.name).toBe(productDto.name)
            expect(product.price).toBe(productDto.price)
        })

        it('Should get product by id', async () => {
            const product = await repository.products.getProductById(2);
            expect(product.name).toBe('product1')
            expect(product.price).toBe(123)
        })

        it('Should list products', async () => {
            const products = await repository.products.getProducts();
            expect(products.length).toBeGreaterThan(1);
        })

        it('Should delete product', async () => {
            const product = await repository.products.getProductById(2);
            const results = await repository.products.getProducts();
            const previousData = [...results];

            await repository.products.deleteProduct(2);

            expect(results).toHaveLength(previousData.length - 1);
            expect(results.find((d: Product) => d.id === product.id)).toBe(undefined)
        })

        it('Should be able to create multiple products', async () => {
            const products: Array<Product> = [
                {
                    id: 3,
                    name: 'product3',
                    price: 450,
                },
                {
                    id: 4,
                    name: 'product4',
                    price: 454,
                },
                {
                    id: 5,
                    name: 'product5',
                    price: 424,
                },
            ];

            await repository.products.createMany(products);
            const results = await repository.products.getProducts();
            expect(results).toHaveLength(4);
        })
    })
})