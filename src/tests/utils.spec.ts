import Generator from '../utils/generators'

describe('Utils', () => {
  it('should generate a random number', async () => {
    const randomNumber = await Generator.generateNumber();
    expect(randomNumber).toBeGreaterThanOrEqual(0);
  })
})
