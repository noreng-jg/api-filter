import request from 'supertest';
import App from '../router'
import Repository from '../repositories';
import Controllers from '../controllers';
import { ProductDTO, Product } from '../models/Product';
import { Tag } from '../models/Tags';

describe('App', () => {
    let app: any;
    let repo: any;
    let cont: any;

    beforeAll(() => {
        repo  = new Repository('fake')
        cont = new Controllers(repo)
        app = new App(cont, 4000)        

        app.init();
    })

    it('/products (GET)', async () => {
        await repo.products.createProduct(<ProductDTO>{name: 'produto1', price: 1720})

        return request(app.instance)
        .get('/products')
        .expect(200)
        .expect((res) => {
            expect(res.body.products[0].name).toBe('produto1')
        })
    })

    it('/products (GET) with query params', async () => {
        let products: Array<Product> = [
           {
            id: 1,
            name: 'product2',
            price: 20,
           },
           {
            id: 3,
            name: 'product3',
            price: 30,
           },
           {
            id: 4,
            name: 'product4',
            price: 24,
           },
           {
            id: 5,
            name: 'product5',
            price: 50,
           },
           {
            id: 6,
            name: 'product6',
            price: 70,
           },
        ]

        await repo.products.createMany(products)

        return request(app.instance)
            .get('/products?page=2&perPage=4')
            .expect(200)
            .expect((res) => {
            expect(res.body.products.length).toBe(2)
            expect(res.body.products[0].name).toBe('product5')
            expect(res.body.products[1].name).toBe('product6')
        })
    })

    it('/products (GET) with query params tags', async () => {
        let products: Array<Product> = [
           {
            id: 10,
            name: 'product2',
            price: 20,
            tags: Array<Tag>({name: 'tag1', id: 1})
           },
           {
            id: 13,
            name: 'product3',
            price: 30,
           },
           {
            id: 14,
            name: 'product4',
            price: 24,
            tags: Array<Tag>({name: 'tag2', id: 2}, {name: 'tag1', id: 1})
           },
           {
            id: 15,
            name: 'product5',
            price: 50,
           },
           {
            id: 16,
            name: 'product6',
            tags: Array<Tag>({name: 'tag2', id: 2}),
            price: 70,
           },
        ]

        await repo.products.createMany(products)

        return request(app.instance)
            .get('/products?page=1&perPage=100&tags[]=tag1&tags[]=tag2')
            .expect(200)
            .expect((res) => {
            expect(res.body.products.length).toBe(1)
        })
    });

    it('/products (GET) with query params tag', () => {
        return request(app.instance)
            .get('/products?page=1&perPage=100&tags[]=tag1')
            .expect(200)
            .expect((res) => {
            expect(res.body.products.length).toBe(2)
        })
    })

    it('/products (GET) with search string', async () => {
        let products: Array<Product> = [
           {
            id: 21,
            name: 'product21',
            price: 20,
            
           },
           {
            id: 23,
            name: 'product33',
            price: 30,
           },
           {
            id: 24,
            name: 'product40',
            price: 24,
            tags: Array<Tag>({name: 'tag2', id: 2}, {name: 'tag1', id: 1})
           },
           {
            id: 25,
            name: 'abd',
            price: 50,
           },
           {
            id: 26,
            name: 'abc',
            tags: Array<Tag>({name: 'tag2', id: 2}),
            price: 70,
           },
        ]

        await repo.products.createMany(products)

        return request(app.instance)
            .get('/products?page=1&perPage=100&search=ab')
            .expect(200)
            .expect((res) => {
            expect(res.body.products.length).toBe(2)
        })
    })
})