import express from 'express';
import multer from 'multer';

const upload = multer({ storage: multer.memoryStorage() })
const router = express.Router();

function fileRouter(fileController: any) {
    const { 
        readFileUploader
    } = fileController;

    router.post('/', upload.single('file'), readFileUploader);

    return router;
}

export {
    fileRouter
};