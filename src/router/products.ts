import express from 'express';

const router = express.Router();

function productRouter(productsController: any) {
    router.get('/', productsController.getProducts);

    return router;
}

export {
    productRouter
};