import express from 'express';
import { fileRouter } from './file'
import { productRouter } from './products';

export default class App {
    private controllers: any;
    private bindport: number;
    public instance: any;

    constructor(controllers: any, bindport: number) {
        this.controllers = controllers;
        this.bindport = bindport;
    }

    init() {
        const app = express();

        app.use(express.json())

        app.use('/upload', fileRouter(this.controllers.file))
        app.use('/products', productRouter(this.controllers.products));

        app.listen(this.bindport, () => {
            console.log(`Listening on port ${this.bindport} ... `)
        })

        this.instance = app;
    }
}