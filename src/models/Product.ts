import { Tag } from './Tags'

interface Product {
  id: number;
  name: string;
  price: number;
  tags?: Tag[];
}

interface ProductDTO {
  name: string;
  price: number;
  tags?: Tag[];
}

interface ProductParams {
  search?: string;
  tags ?: string[];
  page: number;
  perPage: number;
}

const getProductParamsFromQueryString = (query: any): [boolean, ProductParams | undefined ] => {
    if (Reflect.ownKeys(query).length === 0) {
      return [ false, undefined ]
    }

    if (parseInt(query.page) === NaN ) {
      return [ false, undefined ]
    }

    const page = parseInt(query.page)

    if (parseInt(query.perPage) === NaN) {
      return [ false, undefined ]
    }

    const perPage = parseInt(query.perPage)

    const { tags, search } = query;

    if (tags) {
      return [ true, <ProductParams>{
        page,
        perPage,
        tags
      }]
    }

    if (search) {
      return [ true, <ProductParams>{
        page,
        perPage,
        search
      }]
    }

    return [ true, <ProductParams>{
      page,
      perPage
    } ]
}

// export
export { Product, ProductDTO, ProductParams, getProductParamsFromQueryString };
